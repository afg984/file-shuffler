from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(
    packages=[],
    excludes=[],
    include_msvcr=True
)

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('file_shuffler.py', base=base)
]

setup(name='file_shuffler',
      version = '0.1',
      description = 'Shuffle files in a directory',
      options = dict(build_exe = buildOptions),
      executables = executables)
