import re
import sys
import random
import pathlib

from PySide.QtGui import (
    QApplication,
    QDialog, QFileDialog, QMessageBox,
    QLabel, QDialogButtonBox,
    QFormLayout,
)


class FolderSelectDialog(QFileDialog):
    def __init__(self):
        super().__init__(fileMode=QFileDialog.DirectoryOnly)


class MainWindow(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.create_widgets()
        self.link_relations()

    def create_widgets(self):
        self.directory_label = QLabel('未選擇')
        self.status_label = QLabel('未修改')
        self.button_box = QDialogButtonBox()
        self.select_folder_button = self.button_box.addButton(
            '選擇資料夾...',
            QDialogButtonBox.ActionRole,
        )
        self.shuffle_button = self.button_box.addButton(
            '隨機修改檔名',
            QDialogButtonBox.AcceptRole
        )
        self.restore_button = self.button_box.addButton(
            '還原',
            QDialogButtonBox.ResetRole
        )

        self.shuffle_button.setEnabled(False)
        self.restore_button.setEnabled(False)

        layout = QFormLayout()
        layout.addRow('資料夾:', self.directory_label)
        layout.addRow('狀態:', self.status_label)
        layout.addRow(self.button_box)

        self.setLayout(layout)

    def link_relations(self):
        self.select_folder_button.clicked.connect(self.select_folder)
        self.shuffle_button.clicked.connect(self.shuffle)
        self.restore_button.clicked.connect(self.restore)

    def select_folder(self):
        result = QFileDialog.getExistingDirectory()
        self.directory = pathlib.Path(result)
        self.directory_label.setText(result)
        self.shuffle_button.setEnabled(True)
        self.restore_button.setEnabled(True)

    def shuffle(self):
        self.restore(quiet=True)
        targets = [obj for obj in self.directory.glob('*') if obj.is_file()]
        random.shuffle(targets)
        for index, path in enumerate(targets, start=1):
            path.replace(
                path.with_name(
                    '__rnd{index:04d}__{name}'.format(
                        index=index, name=path.name)))
            self.status_label.setText(
                '已修改 {}/{}'.format(index, len(targets)))
        QMessageBox(
            QMessageBox.Information,
            '訊息',
            '已修改 {} 個項目'.format(len(targets)),
        ).exec_()

    def restore(self, quiet=False):
        pattern = re.compile(r'^__rnd\d{4}__')
        contents = [
            obj for obj in self.directory.glob('*')
            if obj.is_file()
        ]
        targets = [path for path in contents if pattern.match(path.name)]
        replacements = [
            path.with_name(pattern.sub('', path.name))
            for path in targets
        ]
        replacement_set = set(replacements)
        if (len(replacement_set) != len(replacements)):
            if quiet:
                return
            QMessageBox(
                QMessageBox.Warning,
                '錯誤',
                '還原取消！還原之後將有相同的檔名'
            ).exec_()
        elif replacement_set.intersection(map(str, contents)):
            if quiet:
                return
            QMessageBox(
                QMessageBox.Warning,
                '錯誤',
                '還原取消！還原之後將有檔案被覆蓋'
            ).exec_()
        elif replacements:
            for index, (path, rep) in enumerate(zip(targets, replacements), 1):
                path.replace(rep)
                self.status_label.setText('已還原 {}/{}'.format(
                    index, len(targets)))
            if not quiet:
                QMessageBox(
                    QMessageBox.Information,
                    '訊息',
                    '已還原 {} 個項目'.format(len(replacements)),
                ).exec_()
        else:
            if quiet:
                return
            QMessageBox(
                QMessageBox.Information,
                '訊息',
                '沒有可以還原的項目'
            ).exec_()


def main():
    application = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    return application.exec_()


if __name__ == '__main__':
    sys.exit(main())